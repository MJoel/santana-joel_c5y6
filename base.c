#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
FILE *file;
FILE *file2;
float proms[24];//promedios

void desvStd(){//muestra en pantalla la media y la desviacion estandard
	int i;
	float sum,med,desv,var,rest,div;
	for(i = 0;i < 24;i++){//suma los promedios
		sum+=proms[i];
	}
	med=sum/24;//se obtiene la media
	printf("Media de los promedios:%.1f\n",med);
	for(i = 0;i < 24;i++){
		rest=(proms[i]-med);//al promedio en la ubicacion i restarle la media
		var+=pow(rest,2);//elevarlo a 2 y sumarlos(sumatoria) 
	}
	div=var/23;//dividir por n-1 (la sumatoria)
	desv=sqrt(div);//obtener la raiz cuadrada de la division y asi obtener la desviacion estandar
	printf("Desviacion estandard:%f\n",desv);
}
void menor(estudiante curso[]){//funcion que muestra en pantalla el promedio menor
	int i,pos,pos2;
	float men=7;
	for(i = 0; i < 24; i++){//compara cada promedio
		if(	men > curso[i].prom){//si el promedio es menor a la nota maxima el valor de la variable men cambia a esa nota
			men = curso[i].prom;
			pos=i;//guardar la posicion para mostrar el nombre y el promedio
		}else
			men = men;
	}
	printf("\nMenor promedio:%s %s %s %.1f \n",curso[pos].nombre,curso[pos].apellidoP,curso[pos].apellidoM,men);
}
void mayor(estudiante curso[]){//funcion que muestra en pantalla el promedio mayor
	int i,pos;
	float may=0;
	for(i = 0; i < 24; i++){
		if(	may < curso[i].prom){//si el promedio es mayor a la nota minima el valor de la variable men cambia a esa nota
			may = curso[i].prom;
			pos=i;//guardar la posicion para mostrar el nombre y el promedio
		}else
			may = may;
	}
	printf("Mayor promedio:%s %s %s %.1f \n",curso[pos].nombre,curso[pos].apellidoP,curso[pos].apellidoM,may);
}

void registroCurso(estudiante curso[]){
	//debe registrar las calificaciones
	int i;
	float promedio;
	for (i = 0; i < 24; i++ ){//mostrar nombres, apellidos y pedir que se ingresen las notas para rellenar los datos de la estructura
		printf("%s  ", curso[i].nombre);
		printf("%s  ", curso[i].apellidoP);
		printf("%s  ", curso[i].apellidoM);
		printf("\nIngrese nota proyecto 1: ");
		scanf("%f", &curso[i].asig_1.proy1);
		printf("Ingrese nota proyecto 2: " );
		scanf("%f", &curso[i].asig_1.proy2);
		printf("Ingrese nota proyecto 3: " );
		scanf("%f", &curso[i].asig_1.proy3);
		printf("Ingrese nota control 1: " );
		scanf("%f", &curso[i].asig_1.cont1);
		printf("Ingrese nota control 2: " );
		scanf("%f", &curso[i].asig_1.cont2);
		printf("Ingrese nota control 3: " );
		scanf("%f", &curso[i].asig_1.cont3);
		printf("Ingrese nota control 4: " );
		scanf("%f", &curso[i].asig_1.cont4);
		printf("Ingrese nota control 5: " );
		scanf("%f", &curso[i].asig_1.cont5);
		printf("Ingrese nota control 6: " );
		scanf("%f", &curso[i].asig_1.cont6);
		//con todas las notas ya colocadas al estudiante se obtiene el promedio 
		promedio=(curso[i].asig_1.proy1*20+curso[i].asig_1.proy2*20+curso[i].asig_1.proy3*30+curso[i].asig_1.cont1*5+curso[i].asig_1.cont2*5+curso[i].asig_1.cont3*5+curso[i].asig_1.cont4*5+curso[i].asig_1.cont5*5+curso[i].asig_1.cont6*5)/100;
		curso[i].prom=promedio;//se asigna el valor del promedio al promedio en la estructura 
		proms[i]=promedio;// se guarda el promedio para su uso en la funcion de desviacion estandar
		printf("%.1f\n",curso[i].prom);
	}
}

void clasificarEstudiantes(char path[], estudiante curso[]){
	int i;
	file=fopen("Aprobados.txt","w");//abrir dos archivos aprobados y reprobados
	file2=fopen("Reprobados.txt","w");
	for(i=0;i<24;i++){
		if(curso[i].prom<4.00)//si el promedio es menor a 4 es reprobado
			fprintf(file2,"%s\t%s\t%s\t%.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM,curso[i].prom);
		else if(curso[i].prom>=4.00)// si el promedio es mayor igual a 4 es aprobado
			fprintf(file,"%s\t%s\t%s\t%.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM,curso[i].prom);
	}
	printf("Clasificados.");
}


void metricasEstudiantes(estudiante curso[]){//esta funcion llama a las funciones para mostrar las metricas de dispersion
	menor(curso);
	mayor(curso);
	desvStd();
}



void menu(estudiante curso[]){
	int opcion,i;
	do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
		switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
			case 5: 
					clasificarEstudiantes("destino", curso);
					fclose(file);
					fclose(file2);
					break;
		 }

	} while ( opcion != 6 );
	for(i=0;i<24;i++)
		proms[i]=0;
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}